

var izbran="";
var x1=400,y1=400,z1=50;

function izbral(kaj){
  switch (kaj) {
    case "sin":
      izbran="sin";
      plot(x1,y1,z1);
      izrisi_sin(x1,y1,z1);
      break;
    case "cos":
      plot(x1,y1,z1);
      izrisi_cos(x1,y1,z1);
      izbran="cos";
      break;
    case "tan":
      plot(x1,y1,z1);
      izrisi_tan(x1,y1,z1);
      izbran="tan";
      break;
    case "cotan":
      plot(x1,y1,z1);
      izrisi_cotan(x1,y1,z1);
      izbran="cotan";
      break;
    default:
      plot(x1,y1,z1);
      break;
}

  }

  function izbrano(){
    switch (izbran) {
      case "sin":
        izbran="sin";
        plot(x1,y1,z1);
        izrisi_sin(x1,y1,z1);
        break;
      case "cos":
        plot(x1,y1,z1);
        izrisi_cos(x1,y1,z1);
        izbran="cos";
        break;
      case "tan":
        plot(x1,y1,z1);
        izrisi_tan(x1,y1,z1);
        izbran="tan";
        break;
      case "cotan":
        plot(x1,y1,z1);
        izrisi_cotan(x1,y1,z1);
        izbran="cotan";
        break;
      default:
        plot(x1,y1,z1);
        break;
  }

    }






function start(){
  var status=false;

  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");

  ctx.canvas.addEventListener('mousemove', function(e){
    if(status==true){
      var mouse_x  = e.pageX-ctx.canvas.offsetLeft;
      var mouse_y  = e.pageY-ctx.canvas.offsetTop;

       console.log("mis na:"+mouse_x+","+mouse_y);

       switch (izbran) {
         case "sin":
            plot(mouse_y,mouse_x,z1);
            izrisi_sin(mouse_y,mouse_x,z1);
           break;
         case "cos":
            plot(mouse_y,mouse_x,z1);
            izrisi_cos(mouse_y,mouse_x,z1);
           break;
         case "tan":
            plot(mouse_y,mouse_x,z1);
            izrisi_tan(mouse_y,mouse_x,z1);
           break;
         case "cotan":
            plot(mouse_y,mouse_x,z1);
            izrisi_cotan(mouse_y,mouse_x,z1);
           break;
         default:
         plot(mouse_y,mouse_x,z1);
         break;
       }

    }
  });



  ctx.canvas.addEventListener('mouseup', function(e){
    status=false;
  });

  ctx.canvas.addEventListener('mousedown', function(e){
    status=true;
  });


plot(x1,y1,z1);


}


function izrisi_cotan(x,y,z){

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var sirina = ctx.canvas.width;
var visina = ctx.canvas.height;
ctx.beginPath();
ctx.strokeStyle = '#ff00ff';
ctx.lineWidth = 3;
  var ofset=0;
  var n=x;
  var m=y;
  var amplituda = z;
  var frekvenca = z;
  ctx.moveTo(0, 0);
  var iter=0;
  var pogoj=false;
  while (iter < sirina) {
    if(pogoj==true){
      var m_1 = Math.sin(((iter+ofset)/frekvenca)*-1);
      var m_2 = Math.cos(((iter+ofset)/frekvenca)*-1);
      m=m_2/m_1;
      m=m*amplituda+x;
      console.log("kordinata   x: "+iter+"| y:"+m+"   iter"+iter);
      ctx.lineTo(iter, m);
      iter++;
    }else if (pogoj==false) {
      while(true){
        m = x+amplituda * Math.sin(((y+ofset)/frekvenca)*-1);
        m2= x+amplituda * Math.sin(((y+2+ofset)/frekvenca)*-1);
        console.log("iscem:"+m );
        if(m<(x+0.5) && m>(x-0.5) && m2<m){
          console.log("našel:"+m );
          pogoj=true;
          break;
        }
        ofset+=1
      }
    }
  }
  ctx.stroke();
}




let sleep = (time) => new Promise(res => setTimeout(res, time));





function izrisi_sin(x,y,z){

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var sirina = ctx.canvas.width;
var visina = ctx.canvas.height;
ctx.beginPath();
ctx.strokeStyle = '#d9ad26';
ctx.lineWidth = 3;
  var ofset=0;
  var n=x;
  var m=y;
  var amplituda = z;
  var frekvenca = z;
  ctx.moveTo(0, 0);
  var iter=0;
  var pogoj=false;
  while (iter < sirina) {
    //await sleep(1);
    if(pogoj==true){
      m = x+amplituda * Math.sin(((iter+ofset)/frekvenca)*-1);
      console.log("kordinata   x: "+iter+"| y:"+m+"   iter"+iter);
      ctx.lineTo(iter, m);
      iter++;
    }else if (pogoj==false) {
      while(true){
        m = x+amplituda * Math.sin(((y+ofset)/frekvenca)*-1);
        m2= x+amplituda * Math.sin(((y+2+ofset)/frekvenca)*-1);
        console.log("iscem:"+m );
        if(m<(x+0.5) && m>(x-0.5) && m2<m){
          console.log("našel:"+m );
          pogoj=true;
          break;
        }
        ofset+=1
      }
    }
    ctx.stroke();
  }

}

function izrisi_cos(x,y,z){
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var sirina = ctx.canvas.width;
var visina = ctx.canvas.height;
ctx.beginPath();
  ctx.strokeStyle = '#00b8a3';
  ctx.lineWidth = 3;
  var ofset=0;
  var n=x;
  var m=y;
  var amplituda = z;
  var frekvenca = z;
  ctx.moveTo(0, 0);
  var iter=0;
  var pogoj=false;
  while (iter < sirina) {
    if(pogoj==true){
      m = x+amplituda * Math.cos(((iter+ofset)/frekvenca)*-1);
      console.log("kordinata   x: "+iter+"| y:"+m+"   iter"+iter);
      ctx.lineTo(iter, m);
      iter++;
    }else if (pogoj==false) {
      while(true){
        m = x+amplituda * Math.cos(((y+ofset)/frekvenca)*-1);
        m2= x+amplituda * Math.cos(((y+2+ofset)/frekvenca)*-1);
        console.log("iscem:"+m );
        if(m<(x+0.5) && m>(x-0.5) && m2>m){
          console.log("našel:"+m );
          pogoj=true;
          break;
        }
        ofset+=1
      }
    }
  }

  ctx.stroke();
}




function izrisi_tan(x,y,z){

var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var sirina = ctx.canvas.width;
var visina = ctx.canvas.height;
ctx.beginPath();
  ctx.strokeStyle = '#00ccff';
  ctx.lineWidth = 3;
  var ofset=0;
  var n=x;
  var m=y;
  var amplituda = z;
  var frekvenca = z;
  ctx.moveTo(0, 0);
  var iter=0;
  var pogoj=false;
  while (iter < sirina) {
    if(pogoj==true){
      m = x+amplituda * Math.tan(((iter+ofset)/frekvenca)*-1);
      console.log("kordinata   x: "+iter+"| y:"+m+"   iter"+iter);
      ctx.lineTo(iter, m);
      iter++;
    }else if (pogoj==false) {
      while(true){
        m = x+amplituda * Math.sin(((y+ofset)/frekvenca)*-1);
        m2= x+amplituda * Math.sin(((y+2+ofset)/frekvenca)*-1);
        console.log("iscem:"+m );
        if(m<(x+0.5) && m>(x-0.5) && m2>m){
          console.log("našel:"+m );
          pogoj=true;
          break;
        }
        ofset++;
      }
    }
      ctx.stroke();
  }
}




function toDegrees (angle) {
  return angle * (180 / Math.PI);
}


function plot(x,y,z){

  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  console.log("start   x: "+x+"| y:"+y);
  var razmik=z;
  var canvas_vel=ctx.canvas.width;

  ctx.beginPath();
  ctx.lineWidth = 5;
  ctx.strokeStyle = '#FF0000';
  ctx.moveTo(0,x);
  ctx.lineTo(canvas_vel,x);
  ctx.stroke();
  ctx.moveTo(y,0);
  ctx.lineTo(y,canvas_vel);
  ctx.stroke();
  ctx.closePath();

ctx.beginPath();
for(i=x+razmik;i<=canvas_vel;i+=razmik){
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#737373";
    ctx.moveTo(0,i);
    ctx.lineTo(canvas_vel,i);
    ctx.stroke();

}
for(i=x-razmik;i>=0;i=i-razmik){
  ctx.moveTo(0,i);
  ctx.lineTo(canvas_vel,i);
  ctx.stroke();

}
for(i=y;i<=canvas_vel;i+=razmik){
  ctx.moveTo(i,0);
  ctx.lineTo(i,canvas_vel);
  ctx.stroke();

}
for(i=y;i>=0;i=i-razmik){
  ctx.moveTo(i,0);
  ctx.lineTo(i,canvas_vel);
  ctx.stroke();

}

}
